/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base;

import comunicacion.SysCom;
import java.net.InetAddress;

/**
 *
 * @author Ulises
 */
public class Machine {
    private InetAddress IP;
    private int puerto;
    private SysCom sc;
    
    public Machine(){}
    public Machine(String name, int portServer ){
     try  {
        this.IP = InetAddress.getByName( name );
        this.puerto = portServer;
        this.sc = new SysCom();
        this.sc.Init(portServer);
     }
     catch(Exception e){
       System.out.println(e);
     }
    }
    
    public void sendMessage(Machine destino, String mensaje){
        
        this.sc.enviar(mensaje.getBytes(), destino.getIP(),destino.getPuertoServer());
    }

    public InetAddress getIP() {
        return IP;
    }

    public void setIP(InetAddress IP) {
        this.IP = IP;
    }

    public int getPuertoServer() {
        return puerto;
    }

    public void setPuertoServer(int puerto) {
        this.puerto = puerto;
    }
    
    
}
