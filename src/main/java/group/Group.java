/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package group;

import base.Machine;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ulises
 */
public class Group {
    
    private String name;
    private List listaMaquinas;
    private int cordinador;
            
    public Group(){
      this.listaMaquinas = new ArrayList();
    }
    
    public Group(String namegroup){
      this.name = namegroup;
      this.listaMaquinas = new ArrayList();
    }
    
    public void addMachine(Machine m){
        this.listaMaquinas.add(m);
    }
    
    public void setcordinador(int cord){
       this.cordinador = cord;
    }
}
